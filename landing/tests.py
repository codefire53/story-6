from django.test import TestCase,LiveServerTestCase,Client
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from .forms import StatusForm
from .models import Status
import chromedriver_binary
# Create your tests here.
class PageTest(TestCase):
    def test_index_exist(self):
        response=Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_output_exist(self):
        response=Client().get('/output/')
        self.assertEqual(response.status_code,200)
    
    def test_error_page(self):
        response=Client().get('/error/')
        self.assertEqual(response.status_code,404)

    def test_index_template(self):
        response=Client().get('/')
        self.assertTemplateUsed(response,'landing.html')
    
    def test_output_template(self):
        response=Client().get('/output/')
        self.assertTemplateUsed(response,'submit.html')
    
    def test_edit_saved(self):
        response=Client().get('/edit/1/save/')
        self.assertEqual(response.status_code,302)

class ModelFormTestCase(TestCase):
    def test_model(self):
        status=Status(status='test')
        status.save()
        obj=Status.objects.get(status='test')
        self.assertEqual(obj.status,'test')

    def test_form(self):
        form_data = {'status': 'test'}
        form =StatusForm(data=form_data)
        self.assertTrue(form.is_valid())
        form_data={}
        form =StatusForm(data=form_data)
        self.assertFalse(form.is_valid())


class UnitTestCase(TestCase):
    def test_submit(self):
        response=Client().post('/submit/',{'status':'Test'})
        stat=Status.objects.get(status='Test')
        self.assertEqual(stat.status,'Test')
        self.assertEqual(response.status_code,302)

    def test_edit(self):
        stat=Status(status='dummy')
        stat.save()
        response=Client().post('/edit/1/save/',{'status':'Test'})
        stat=Status.objects.get(id=1)
        self.assertEqual(response.status_code,302)
        self.assertEqual(stat.status,'Test')

    def test_invalid_edit(self):
        response=Client().post('/edit/1/save/',{'status':'Test'})
        self.assertEqual(response.status_code,200)
        self.assertIn(b'Status tidak ditemukan :(',response.content)


class FunctionalTestCase(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefectch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path='./chromedriver.log'
        service_args=['--verbose']
        self.selenium = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
        #self.selenium=webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()

    def test_register(self):        
        selenium=self.selenium  
        selenium.get('http://localhost:8000/output/')
        self.assertIn('Anda belum mengisi status apapun OwO',selenium.page_source) 
        selenium.get("http://localhost:8000/")
        status=selenium.find_element_by_id('id_status')
        status.send_keys('Coba Coba')
        selenium.find_element_by_name('submit').click()
        selenium.find_element_by_name('status').click()
        selenium.get('http://localhost:8000/output/')
        self.assertIn('Coba Coba',selenium.page_source)
        status=selenium.find_element_by_id('id_status')
        status.send_keys('Coba')
        change=selenium.find_element_by_id('simpan')
        change.click()
        self.assertIn('Coba',selenium.page_source)

