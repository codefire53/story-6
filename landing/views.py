from django.shortcuts import render,redirect
from .forms import StatusForm,EditForm
from .models import Status
from django.http import HttpResponse
# Create your views here.
def index(request):
    response={'status_form':StatusForm}
    return render (request,'landing.html',response)

def submit_form(request):
    if (request.method=="POST"):
        form=StatusForm(request.POST)
        if(form.is_valid()):
            status=Status(status=form.cleaned_data['status'])
            status.save()
    return redirect('/')

def out(request):
    response={'form':EditForm}
    response['output']=Status.objects.all()
    return render(request,'submit.html',response)


def edit_status(request,item_id):
    if(request.method=="POST"):
        form=StatusForm(request.POST)
        if(form.is_valid()):
            try:
                stat=Status.objects.get(id=item_id)
                stat.status=form.cleaned_data['status']
                stat.save()
                return redirect('/output/')
            except Status.DoesNotExist:
                return HttpResponse('Status tidak ditemukan :(')
    return redirect('/output/'.format(item_id))
