from django.contrib import admin
from django.urls import path
from .views import index,submit_form,out,edit_status
urlpatterns = [
    path('', index,name="index"),
    path('output/',out,name='output'),
    path('submit/',submit_form,name='submit_form'),
    path('edit/<item_id>/save/',edit_status,name='edit_status')
]