from django import forms
class StatusForm(forms.Form):
    attrs={
        'type':'text',
        'class': 'form-control',
    }
    status=forms.CharField(max_length=300,required=True,label="",widget=forms.TextInput(attrs=attrs))
class EditForm(forms.Form):
    attrs={
        'type':'text',
        'class': 'form-control'
    }
    status=forms.CharField(max_length=300,required=False,label="",widget=forms.TextInput(attrs=attrs))